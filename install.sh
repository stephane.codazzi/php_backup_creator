#!/bin/bash

function usage {
  echo -e "\nUsage ./docker.sh <command>\n"
  echo -e "Available commands:\n"

  echo -e "- install: Purge, restart and install environment"
  echo -e "- install-ci: Purge, restart and install environment without dev packages"
  echo -e "- console: Enter in docker console"
  echo -e "- purge: Stop and purge dockers"
  echo -e ""

  echo -e "- console-root: Enter in docker console with root user"
  echo -e "- console-localstack: Enter in localstack docker"
  echo -e "- run <cmd>: Execute command in php docker"
  echo -e "- exec_in <path> <cmd>: Execute command in docker"
  echo -e "- logs: Show logs"
  echo -e "- ps: List dockers"
  echo -e "- start: Start dockers"
  echo -e ""
}

function console {
  env UID=${UID} ${DOCKER_COMPOSE} run php bash
}
function console-root {
  env UID=${UID} ${DOCKER_COMPOSE} exec -u root php bash
}
function console-localstack {
  env UID=${UID} ${DOCKER_COMPOSE} exec -u localstack localstack bash
}
function run {
  env UID=${UID} ${DOCKER_COMPOSE} run -T php $EX_CMD
}
function exec_in {
  echo -e "$EX_CMD"
  env UID=${UID} ${DOCKER_COMPOSE} exec -T -w $EX_PATH php $EX_CMD
}
function purge {
  env UID=${UID} ${DOCKER_COMPOSE} down --remove-orphans -v
	env UID=${UID} ${DOCKER_COMPOSE} rm -f -v
}
function ps {
  env UID=${UID} ${DOCKER_COMPOSE} ps
}
function logs {
  env UID=${UID} ${DOCKER_COMPOSE} logs -tf
}
function start {
  env UID=${UID} ${DOCKER_COMPOSE} pull
  env UID=${UID} ${DOCKER_COMPOSE} up --build -d
}
function install {
  purge
  start
  env UID=${UID} ${DOCKER_COMPOSE} run -T php bash bin/waitMysql.sh
  env UID=${UID} ${DOCKER_COMPOSE} run -T php mysql -u root -proot -h mysql < docker/data/mysql.sql
  env UID=${UID} ${DOCKER_COMPOSE} run -T php bash docker/data/importPostgresql.sh
  env UID=${UID} ${DOCKER_COMPOSE} run -T php composer install
  env UID=${UID} ${DOCKER_COMPOSE} exec -T swift mkdir -p /data/storage
  env UID=${UID} ${DOCKER_COMPOSE} exec -u localstack localstack awslocal s3api create-bucket --bucket backups
  console
}
function install-ci {
  purge
  start
  env UID=${UID} ${DOCKER_COMPOSE} run -T php bash bin/waitMysql.sh
  env UID=${UID} ${DOCKER_COMPOSE} run -T php mysql -u root -proot -h mysql < docker/data/mysql.sql
  env UID=${UID} ${DOCKER_COMPOSE} run -T php bash docker/data/importPostgresql.sh
  env UID=${UID} ${DOCKER_COMPOSE} run -T php composer install --no-dev
}

## MAIN FUNCTIONS
case $ENV in
  ci)
    DOCKER_COMPOSE="docker compose -p project_$JOB_ID -f docker/docker-compose-ci.yml"
    ;;
  *)
    DOCKER_COMPOSE="docker compose -p project -f docker/docker-compose.yml"
    ;;
esac

DOCKER_FILE=docker/.docker.conf
if [ ! -f "$DOCKER_FILE" ]; then
    echo "You need to create $DOCKER_FILE file !"
    exit 1
fi

case $1 in
  install)
    install
    ;;
  install.sh)
    install
    ;;
  install-ci)
    install-ci
    ;;
  purge)
    purge
    ;;
  ps)
    ps
    ;;
  logs)
    logs
    ;;
  run)
    EX_CMD="$2"
    run
    ;;
  exec_in)
    EX_PATH="$2"
    EX_CMD="$3"
    exec_in
    ;;
  console-root)
    console-root
    ;;
  console-localstack)
    console-localstack
    ;;
  console)
    console
    ;;
  start)
    start
    ;;
  *)
    usage
    ;;
esac
