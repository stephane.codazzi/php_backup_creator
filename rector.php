<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\CodeQuality\Rector\Identical\FlipTypeControlToUseExclusiveTypeRector;
use Rector\CodeQuality\Rector\Ternary\SwitchNegatedTernaryRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;
use Rector\Strict\Rector\Empty_\DisallowedEmptyRuleFixerRector;
use Rector\Symfony\Set\SymfonySetList;
use Rector\TypeDeclaration\Rector\ClassMethod\StrictArrayParamDimFetchRector;

return RectorConfig::configure()
    ->withPreparedSets(
        codeQuality: true,
        codingStyle: true,
        typeDeclarations: true
    )
    ->withAttributesSets(
        symfony: true,
    )
    ->withPaths([
        __DIR__.'/config',
        __DIR__.'/src',
    ])
    ->withRootFiles()
    ->withRules([
        InlineConstructorDefaultToPropertyRector::class,
    ])
    ->withSkip([
        FlipTypeControlToUseExclusiveTypeRector::class,
        StrictArrayParamDimFetchRector::class,
        DisallowedEmptyRuleFixerRector::class,
        SwitchNegatedTernaryRector::class,
    ])
    ->withPhpSets(php83: true)
    ->withSets([
        SetList::PHP_83,

        SymfonySetList::CONFIGS,
        SymfonySetList::SYMFONY_72,
        SymfonySetList::SYMFONY_CODE_QUALITY,
        SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
    ]);
