SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `database1`
--
CREATE DATABASE IF NOT EXISTS `database1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `database1`;

-- --------------------------------------------------------

--
-- Structure de la table `table1`
--

CREATE TABLE `table1` (
  `id` int(13) NOT NULL,
  `name` varchar(254) NOT NULL,
  `description` text NOT NULL,
  `rank` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `table1`
--

INSERT INTO `table1` (`id`, `name`, `description`, `rank`) VALUES
(1, 'Test', 'My test line', 13),
(2, 'Test', 'My test line', 13),
(3, 'Test', 'My test line', 13);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `table1`
--
ALTER TABLE `table1`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `database2`
--
CREATE DATABASE IF NOT EXISTS `database2` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `database2`;

-- --------------------------------------------------------

--
-- Structure de la table `table1`
--

CREATE TABLE `table1` (
                          `id` int(13) NOT NULL,
                          `name` varchar(254) NOT NULL,
                          `description` text NOT NULL,
                          `rank` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `table1`
--

INSERT INTO `table1` (`id`, `name`, `description`, `rank`) VALUES
(1, 'Test', 'My test line', 13),
(2, 'Test', 'My test line', 13),
(3, 'Test', 'My test line', 13);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `table1`
--
ALTER TABLE `table1`
    ADD PRIMARY KEY (`id`);

--