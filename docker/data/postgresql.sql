--
-- PostgreSQL database dump
--

-- Dumped from database version 11.11 (Debian 11.11-1.pgdg90+1)
-- Dumped by pg_dump version 11.10 (Debian 11.10-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mytable; Type: TABLE; Schema: public; Owner: project
--

CREATE TABLE public.mytable (
                                id integer NOT NULL,
                                name character varying(32),
                                description character varying(128)
);


ALTER TABLE public.mytable OWNER TO project;

--
-- Name: mytable_id_seq; Type: SEQUENCE; Schema: public; Owner: project
--

CREATE SEQUENCE public.mytable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mytable_id_seq OWNER TO project;

--
-- Name: mytable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: project
--

ALTER SEQUENCE public.mytable_id_seq OWNED BY public.mytable.id;


--
-- Name: mytable id; Type: DEFAULT; Schema: public; Owner: project
--

ALTER TABLE ONLY public.mytable ALTER COLUMN id SET DEFAULT nextval('public.mytable_id_seq'::regclass);


--
-- Data for Name: mytable; Type: TABLE DATA; Schema: public; Owner: project
--

COPY public.mytable (id, name, description) FROM stdin;
1	first	My first line
2	second	My second line
\.


--
-- Name: mytable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: project
--

SELECT pg_catalog.setval('public.mytable_id_seq', 2, true);


--
-- PostgreSQL database dump complete
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.11 (Debian 11.11-1.pgdg90+1)
-- Dumped by pg_dump version 11.10 (Debian 11.10-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mytable; Type: TABLE; Schema: public; Owner: project
--

CREATE TABLE public.mytable (
                                id integer NOT NULL,
                                name character varying(32),
                                description character varying(128)
);


ALTER TABLE public.mytable OWNER TO project;

--
-- Name: mytable_id_seq; Type: SEQUENCE; Schema: public; Owner: project
--

CREATE SEQUENCE public.mytable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mytable_id_seq OWNER TO project;

--
-- Name: mytable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: project
--

ALTER SEQUENCE public.mytable_id_seq OWNED BY public.mytable.id;


--
-- Name: mytable id; Type: DEFAULT; Schema: public; Owner: project
--

ALTER TABLE ONLY public.mytable ALTER COLUMN id SET DEFAULT nextval('public.mytable_id_seq'::regclass);


--
-- Data for Name: mytable; Type: TABLE DATA; Schema: public; Owner: project
--

COPY public.mytable (id, name, description) FROM stdin;
1	first	My first line
2	second	My second line
\.


--
-- Name: mytable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: project
--

SELECT pg_catalog.setval('public.mytable_id_seq', 2, true);


--
-- PostgreSQL database dump complete
--
