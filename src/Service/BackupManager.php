<?php

namespace App\Service;

use App\Exception\CompressionException;
use App\Exception\ConfigException;
use App\Exception\SystemProcessException;
use App\Service\Backup\BackupInterface;
use App\Service\Compression\CompressionInterface;
use App\Service\Sync\SyncInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;

class BackupManager
{
    /** @var BackupInterface[] */
    private array $backupServices = [];

    /** @var SyncInterface[] */
    private array $syncServices = [];

    /** @var CompressionInterface[] */
    private array $compressionServices = [];

    private readonly string $temporaryDumpFolder;

    private string $backupFilename = '';

    private string $backupFilenameTemplate = '';

    /**
     * BackupManager constructor.
     *
     * @param iterable<BackupInterface>      $backupServices
     * @param iterable<SyncInterface>        $syncServices
     * @param iterable<CompressionInterface> $compressionServices
     */
    public function __construct(
        iterable $backupServices,
        iterable $syncServices,
        iterable $compressionServices,
        private readonly string $dumpFolder,
        private readonly LoggerInterface $logger,
        private readonly NotificationManager $notificationManager,
        private readonly SystemProcessManager $systemProcess,
    ) {
        foreach ($backupServices as $backupService) {
            $this->backupServices[] = $backupService;
        }

        foreach ($syncServices as $syncService) {
            $this->syncServices[] = $syncService;
        }

        foreach ($compressionServices as $compressionService) {
            $this->compressionServices[] = $compressionService;
        }

        $this->temporaryDumpFolder = $this->dumpFolder.DIRECTORY_SEPARATOR.'tmp';
    }

    /**
     * @throws SystemProcessException
     * @throws \Exception
     */
    public function startBackup(string $configPath): void
    {
        if (is_dir($this->dumpFolder)) {
            $this->systemProcess->execProcess(['rm', '-rf', $this->dumpFolder]);
        }

        if (false === mkdir($this->dumpFolder)) {
            throw new \Exception('Cannot create backup folder');
        }

        if (false === mkdir($this->temporaryDumpFolder)) {
            throw new \Exception('Cannot create temporary backup folder');
        }

        $configuration = $this->readConfiguration($configPath);
        $this->loadServices($configuration);
        $this->backupServices();
        $this->compressBackup();
        $this->syncBackup();
        $this->purgeOldFiles();
    }

    /**
     * Read configuration file.
     *
     * @return array<mixed>
     *
     * @throws \Exception
     */
    private function readConfiguration(string $configPath): array
    {
        $configuration = Yaml::parseFile($configPath);

        if (!is_array($configuration)) {
            throw new \Exception('Cannot parse config file');
        }

        return $configuration;
    }

    /**
     * Init services.
     *
     * @param array<mixed> $configuration
     *
     * @throws ConfigException
     */
    public function loadServices(array $configuration): void
    {
        foreach ($this->backupServices as $backupService) {
            $backupService->load($configuration);
        }

        foreach ($this->compressionServices as $compressService) {
            $compressService->load($configuration);
        }

        foreach ($this->syncServices as $syncService) {
            $syncService->load($configuration);
        }
    }

    private function backupServices(): void
    {
        foreach ($this->backupServices as $backupService) {
            try {
                if ($backupService->isEnable()) {
                    $this->logger->info(sprintf('Start backup service: %s', $backupService->getServiceName()));
                    $backupService->backup($this->temporaryDumpFolder);
                }
            } catch (\Exception $e) {
                // Do not stop backup
                $this->notificationManager->captureException($e);
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * @throws CompressionException
     */
    private function compressBackup(): void
    {
        foreach ($this->compressionServices as $compressionService) {
            // In case of compression fail, the backup is stopped: not possible to synchronize
            if ($compressionService->isEnable()) {
                $this->logger->info(sprintf('Start compression service: %s', $compressionService->getServiceName()));
                $compressionService->compress($this->dumpFolder, $this->temporaryDumpFolder);
                $this->backupFilename = $compressionService->getFinalFilename();
                $this->backupFilenameTemplate = $compressionService->getFilenameTemplate();
            }
        }
    }

    private function syncBackup(): void
    {
        foreach ($this->syncServices as $syncService) {
            try {
                if ($syncService->isEnable()) {
                    $this->logger->info(sprintf('Start sync service: %s', $syncService->getServiceName()));
                    $syncService->sync($this->dumpFolder, $this->backupFilename);
                }
            } catch (\Exception $e) {
                // Do not stop sync
                $this->notificationManager->captureException($e);
                $this->logger->error($e->getMessage());
            }
        }
    }

    private function purgeOldFiles(): void
    {
        foreach ($this->syncServices as $syncService) {
            try {
                if ($syncService->isEnable()) {
                    $this->logger->info(sprintf('Start purge service: %s', $syncService->getServiceName()));
                    $syncService->purgeOldFiles($this->backupFilenameTemplate);
                }
            } catch (\Exception $e) {
                // Do not stop purge
                $this->notificationManager->captureException($e);
                $this->logger->error($e->getMessage());
            }
        }
    }
}
