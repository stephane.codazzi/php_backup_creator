<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Sentry\ClientInterface;
use Sentry\State\Scope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Process\Process;

class NotificationManager
{
    /**
     * NotificationManager constructor.
     *
     * @param array<string> $emails
     */
    public function __construct(private readonly array $emails, private readonly string $mailerFrom, private readonly ClientInterface $sentry, private readonly MailerInterface $mailer, private readonly LoggerInterface $logger)
    {
    }

    /**
     * @param array<mixed> $extraContent
     */
    public function captureException(\Exception $e, array $extraContent = []): void
    {
        $scope = new Scope();
        $scope->setExtras($extraContent);

        $this->sentry->captureException($e);
        $this->logger->error($e->getMessage());
        $this->sendEmail('[BACKUP CREATOR] New exception', $e->getMessage());
    }

    public function captureProcessException(\Exception $e, Process $process): void
    {
        $extra = [
            'command' => $process->getCommandLine(),
            'output' => $process->getOutput(),
            'error' => $process->getErrorOutput(),
        ];

        $scope = new Scope();
        $scope->setExtras($extra);

        $this->sentry->captureException($e, $scope);
        $this->logger->error($e->getMessage());
        $this->logger->error($process->getCommandLine());
        $this->logger->error($process->getErrorOutput());
        $this->sendEmail(
            '[BACKUP CREATOR] New exception',
            sprintf(
                "Exception: %s\nCommand: %s\nError: %s",
                $e->getMessage(),
                $process->getCommandLine(),
                $process->getErrorOutput()
            )
        );
    }

    public function sendEmail(string $subject, string $body): void
    {
        if ([] === $this->emails || empty($this->mailerFrom)) {
            $this->logger->notice('Email notification is disabled');

            return;
        }

        try {
            $email = (new Email())
                ->from($this->mailerFrom)
                ->subject($subject)
                ->text($body)
            ;

            foreach ($this->emails as $emailAddress) {
                $email->addTo($emailAddress);
            }

            $this->mailer->send($email);
        } catch (\Exception $exception) {
            $this->logger->error('[EMAIL ERROR]: '.$exception->getMessage());
        }
    }
}
