<?php

namespace App\Service;

use App\Exception\ConfigException;
use App\Exception\DependencyException;
use App\Exception\SystemProcessException;
use Psr\Log\LoggerInterface;

abstract class BaseService implements ServiceInterface
{
    public const TYPE_BACKUP = 'backup';

    public const TYPE_COMPRESSION = 'compression';

    public const TYPE_SYNC = 'sync';

    protected bool $enabled = false;

    /**
     * BaseService constructor.
     */
    public function __construct(protected LoggerInterface $logger, protected NotificationManager $notification, protected SystemProcessManager $systemProcess)
    {
    }

    /**
     * Function used to check service dependencies.
     *
     * @throws DependencyException
     */
    abstract protected function checkDependencies(): void;

    /**
     * Function used to load service configuration.
     *
     * @param array<mixed> $serviceConfiguration Service configuration
     *
     * @throws ConfigException
     */
    abstract protected function parseConfiguration(array $serviceConfiguration): void;

    public function load(array $configuration): void
    {
        if (self::TYPE_COMPRESSION === $this->getServiceType()) {
            $this->loadCompressionService($configuration);

            return;
        }

        $this->loadGenericService($configuration);
    }

    /**
     * @param array<mixed> $configuration
     *
     * @throws ConfigException
     */
    private function loadGenericService(array $configuration): void
    {
        if (empty($configuration[$this->getServiceType()])
            || empty($configuration[$this->getServiceType()][$this->getServiceName()])
            || empty($configuration[$this->getServiceType()][$this->getServiceName()]['enabled'])
            || true !== $configuration[$this->getServiceType()][$this->getServiceName()]['enabled']) {
            return;
        }

        $this->enabled = true;
        $this->checkDependencies();
        $this->parseConfiguration($configuration[$this->getServiceType()][$this->getServiceName()]);
    }

    /**
     * @param array<mixed> $configuration
     *
     * @throws ConfigException
     */
    private function loadCompressionService(array $configuration): void
    {
        if (empty($configuration[$this->getServiceType()])
            || empty($configuration[$this->getServiceType()]['type'])
            || empty($configuration[$this->getServiceType()]['type'])
            || $this->getServiceName() !== $configuration[$this->getServiceType()]['type']) {
            return;
        }

        $this->enabled = true;
        $this->checkDependencies();
        $this->parseConfiguration($configuration[$this->getServiceType()]);
    }

    /**
     * @throws DependencyException
     */
    protected function checkServerExecutable(string $executable): void
    {
        try {
            $this->systemProcess->execProcessCommandLine(sprintf('type %s > /dev/null', $executable));
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                sprintf('Missing command: %s', $executable),
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );
            $this->notification->captureProcessException($exception, $systemProcessException->getProcess());

            throw $exception;
        }
    }

    public function isEnable(): bool
    {
        return $this->enabled;
    }
}
