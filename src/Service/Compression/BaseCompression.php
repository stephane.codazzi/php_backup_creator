<?php

namespace App\Service\Compression;

use App\Exception\ConfigException;
use App\Service\BaseService;

abstract class BaseCompression extends BaseService implements CompressionInterface
{
    protected string $dateFormat = '';

    protected string $compressedFilename = '';

    protected string $baseFileName = '';

    protected string $extension = '';

    /** @var array<string|int> */
    protected array $options = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceType(): string
    {
        return self::TYPE_COMPRESSION;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilenameTemplate(): string
    {
        return $this->baseFileName;
    }

    /**
     * {@inheritdoc}
     */
    public function getFinalFilename(): string
    {
        return $this->compressedFilename.'.'.$this->extension;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['type'])) {
            throw new ConfigException($this->getServiceName(), 'compression.type is missing');
        }

        if ($this->getServiceName() !== $serviceConfiguration['type']) {
            return;
        }

        $this->enabled = true;

        if (empty($serviceConfiguration['name'])) {
            throw new ConfigException($this->getServiceName(), 'compression.name is missing');
        }

        $this->compressedFilename = $serviceConfiguration['name'];

        if (empty($serviceConfiguration['extension'])) {
            throw new ConfigException($this->getServiceName(), 'compression.extension is missing');
        }

        $this->extension = $serviceConfiguration['extension'];

        if (str_contains((string) $this->compressedFilename, '{date}')) {
            if (empty($serviceConfiguration['dateFormat'])) {
                throw new ConfigException($this->getServiceName(), 'compression.dateFormat used but missing');
            }

            $this->baseFileName = str_replace('{date}', '', $this->compressedFilename);

            $date = (new \DateTimeImmutable())->format($serviceConfiguration['dateFormat']);
            $this->compressedFilename = str_replace('{date}', $date, $this->compressedFilename);
        }

        if (!empty($serviceConfiguration['options'])) {
            if (!is_array($serviceConfiguration['options'])) {
                throw new ConfigException($this->getServiceName(), 'compression.options must be an array');
            }

            $this->options = $serviceConfiguration['options'];
        }
    }
}
