<?php

namespace App\Service\Compression;

use App\Exception\CompressionException;
use App\Service\ServiceInterface;

interface CompressionInterface extends ServiceInterface
{
    /**
     * Start compression service.
     *
     * @throws CompressionException
     */
    public function compress(string $backupFinalFolder, string $backupTemporaryFolder): void;

    /**
     * Get the final filename with compression extension.
     *
     * @return string Backup filename
     */
    public function getFinalFilename(): string;

    /**
     * Get filename template, without extension, and without variable like {date}.
     * For example, "my_backup_{date}" return "my_backup_".
     * Used for prune old files.
     *
     * @return string Backup filename template
     */
    public function getFilenameTemplate(): string;
}
