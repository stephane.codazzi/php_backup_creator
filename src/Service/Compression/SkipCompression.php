<?php

namespace App\Service\Compression;

class SkipCompression extends BaseCompression
{
    private const string SERVICE_NAME = 'skip';

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function getFinalFilename(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function compress(string $backupFinalFolder, string $backupTemporaryFolder): void
    {
    }
}
