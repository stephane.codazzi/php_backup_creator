<?php

namespace App\Service\Compression;

use App\Exception\CompressionException;
use App\Exception\DependencyException;
use App\Exception\SystemProcessException;

class ZipCompression extends BaseCompression
{
    private const string SERVICE_NAME = 'zip';

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    #[\Override]
    public function getFinalFilename(): string
    {
        return $this->compressedFilename.'.zip';
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
        try {
            $this->systemProcess->execProcessCommandLine('type zip > /dev/null');
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                'Missing command: zip',
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );

            $this->notification->captureProcessException(
                $exception,
                $systemProcessException->getProcess()
            );

            throw $exception;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function compress(string $backupFinalFolder, string $backupTemporaryFolder): void
    {
        $cmd = [
            'zip',
            '-r',
        ];

        foreach ($this->options as $option) {
            $cmd[] = $option;
        }

        $cmd[] = $backupFinalFolder.DIRECTORY_SEPARATOR.$this->getFinalFilename();
        $cmd[] = '.';

        try {
            $this->systemProcess->execProcess($cmd, 18000, [], $backupTemporaryFolder);
        } catch (SystemProcessException $systemProcessException) {
            $exception = new CompressionException(
                $this->getServiceName(),
                'Cannot compress backup',
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );

            $this->notification->captureProcessException($exception, $systemProcessException->getProcess());

            throw $exception;
        }
    }
}
