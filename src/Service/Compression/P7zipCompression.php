<?php

namespace App\Service\Compression;

use App\Exception\CompressionException;
use App\Exception\DependencyException;
use App\Exception\SystemProcessException;

class P7zipCompression extends BaseCompression
{
    private const string SERVICE_NAME = 'p7zip';

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
        try {
            $this->systemProcess->execProcessCommandLine('type 7z > /dev/null');
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                'Missing command: 7z',
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );

            $this->notification->captureProcessException(
                $exception,
                $systemProcessException->getProcess()
            );

            throw $exception;
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws SystemProcessException
     */
    public function compress(string $backupFinalFolder, string $backupTemporaryFolder): void
    {
        $cmd = [
            '7z',
            'a',
            '-bd',
        ];

        foreach ($this->options as $option) {
            $cmd[] = $option;
        }

        $cmd[] = $backupFinalFolder.DIRECTORY_SEPARATOR.$this->getFinalFilename();
        $cmd[] = '.';

        try {
            $this->systemProcess->execProcess($cmd, 18000, [], $backupTemporaryFolder);
        } catch (SystemProcessException $systemProcessException) {
            $this->notification->captureProcessException(
                new CompressionException(
                    $this->getServiceName(),
                    'Cannot compress backup',
                    (int) $systemProcessException->getCode(),
                    $systemProcessException
                ),
                $systemProcessException->getProcess()
            );

            throw $systemProcessException;
        }
    }
}
