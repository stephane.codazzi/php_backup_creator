<?php

namespace App\Service\Sync;

use App\Exception\ConfigException;
use App\Exception\DependencyException;
use App\Exception\SyncException;
use App\Exception\SystemProcessException;
use App\Model\Sync\RCloneEnvironment;

class RClone extends BaseSync
{
    private const string SERVICE_NAME = 'rclone';

    private string $executable = '';

    /** @var RCloneEnvironment[] */
    private array $environments = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['executable'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone need to contain executable field');
        }

        $this->executable = $serviceConfiguration['executable'];

        if (empty($serviceConfiguration['environments']) || !is_array($serviceConfiguration['environments'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone need to contain environments array');
        }

        foreach ($serviceConfiguration['environments'] as $environment) {
            $this->environments[] = $this->parseEnvironment($environment);
        }
    }

    /**
     * @param array<mixed> $environment
     *
     * @throws ConfigException
     */
    private function parseEnvironment(array $environment): RCloneEnvironment
    {
        $additionalOptions = [];

        if (empty($environment['configFilePath'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone.environment need to contain configFilePath');
        }

        if (empty($environment['filesToKeep'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone.environment need to contain filesToKeep field');
        }

        if (empty($environment['remote'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone.environment need to contain remote');
        }

        if (empty($environment['destination'])) {
            throw new ConfigException($this->getServiceName(), 'sync.rclone.environment need to contain destination');
        }

        if (!empty($environment['additionalOptions'])) {
            if (!is_array($environment['additionalOptions'])) {
                throw new ConfigException($this->getServiceName(), 'sync.rclone.environment.additionalOptions need to be a string array');
            }

            foreach ($environment['additionalOptions'] as $additionalOption) {
                if (!is_string($additionalOption)) {
                    throw new ConfigException($this->getServiceName(), 'sync.rclone.environment.additionalOptions need to be a string array');
                }

                $additionalOptions[] = $additionalOption;
            }
        }

        return new RCloneEnvironment(
            $environment['configFilePath'],
            $environment['remote'],
            $environment['destination'],
            $additionalOptions,
            $environment['filesToKeep']
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
        try {
            $this->systemProcess->execProcessCommandLine(sprintf('type %s > /dev/null', $this->executable));
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                'Missing executable: '.$this->executable,
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );

            $this->notification->captureProcessException(
                $exception,
                $systemProcessException->getProcess()
            );

            throw $exception;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sync(string $backupPath, string $backupFilename): void
    {
        foreach ($this->environments as $environment) {
            try {
                $cmd = [
                    $this->executable,
                    'copy',
                    ...$environment->getAdditionalOptions(),
                    '--config',
                    $environment->getConfigFilePath(),
                    $backupPath.DIRECTORY_SEPARATOR.$backupFilename,
                    sprintf('%s:%s', $environment->getRemote(), $environment->getDestination()),
                ];

                $this->systemProcess->execProcess($cmd, 3600);
            } catch (SystemProcessException $e) {
                $exception = new SyncException(
                    $this->getServiceName(),
                    sprintf(
                        'Cannot sync in destination: %s:%s',
                        $environment->getRemote(),
                        $environment->getDestination()
                    ),
                    (int) $e->getCode(),
                    $e
                );

                $this->notification->captureProcessException(
                    $exception,
                    $e->getProcess()
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function purgeOldFiles(string $backupFilenameTemplate): void
    {
        foreach ($this->environments as $environment) {
            try {
                $cmd = [
                    $this->executable,
                    'lsf',
                    '--config',
                    $environment->getConfigFilePath(),
                    sprintf('%s:%s', $environment->getRemote(), $environment->getDestination()),
                ];

                $commandFiles = $this->systemProcess->execProcess($cmd, 3600);
            } catch (SystemProcessException $e) {
                $exception = new SyncException(
                    $this->getServiceName(),
                    sprintf('Cannot list rclone files in environment container: %s', $environment->getRemote()),
                    (int) $e->getCode(),
                    $e
                );

                $this->notification->captureProcessException(
                    $exception,
                    $e->getProcess()
                );

                continue;
            }

            $files = explode(PHP_EOL, $commandFiles);
            $filesToDelete = $this->getFilesToDelete($files, $backupFilenameTemplate, $environment->getFilesToKeep());

            foreach ($filesToDelete as $file) {
                try {
                    $cmd = [
                        $this->executable,
                        'deletefile',
                        '--config',
                        $environment->getConfigFilePath(),
                        sprintf('%s:%s/%s', $environment->getRemote(), $environment->getDestination(), $file),
                    ];
                    $this->systemProcess->execProcess($cmd, 3600);
                } catch (SystemProcessException $e) {
                    $exception = new SyncException(
                        $this->getServiceName(),
                        sprintf('Cannot delete rclone file (%s) in environment: %s', $file, $environment->getRemote()),
                        (int) $e->getCode(),
                        $e
                    );

                    $this->notification->captureProcessException(
                        $exception,
                        $e->getProcess()
                    );
                }
            }
        }
    }
}
