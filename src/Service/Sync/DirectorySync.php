<?php

namespace App\Service\Sync;

use App\Exception\ConfigException;
use App\Exception\SyncException;
use App\Exception\SystemProcessException;
use App\Model\Sync\Directory;

class DirectorySync extends BaseSync
{
    private const string SERVICE_NAME = 'directory';

    /** @var array<Directory> */
    private array $folders = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['folders']) || !is_array($serviceConfiguration['folders'])) {
            throw new ConfigException($this->getServiceName(), 'sync.directory need to contain folders array');
        }

        foreach ($serviceConfiguration['folders'] as $folder) {
            if (empty($folder['path'])) {
                throw new ConfigException($this->getServiceName(), 'sync.directory.folders need to contain array with "path" field');
            }

            if (empty($folder['filesToKeep'])) {
                throw new ConfigException($this->getServiceName(), 'sync.directory.folders need to contain array with "filesToKeep" field');
            }

            $directory = new Directory();
            $directory->setPath($folder['path']);
            $directory->setFilesToKeep($folder['filesToKeep']);

            $this->folders[] = $directory;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function sync(string $backupPath, string $backupFilename): void
    {
        foreach ($this->folders as $folder) {
            if (!is_dir($folder->getPath()) && false === mkdir($folder->getPath())) {
                $exception = new SyncException(
                    $this->getServiceName(),
                    sprintf('Cannot create backup folder: %s', $folder->getPath())
                );

                $this->notification->captureException($exception, ['path' => $folder->getPath()]);
                continue;
            }

            $cmd = ['cp', $backupPath.DIRECTORY_SEPARATOR.$backupFilename, $folder->getPath()];

            try {
                $this->systemProcess->execProcess($cmd);
            } catch (SystemProcessException $e) {
                $exception = new SyncException(
                    $this->getServiceName(),
                    'Cannot copy in folder: %s'.$folder->getPath(),
                    (int) $e->getCode(),
                    $e
                );

                $this->notification->captureProcessException(
                    $exception,
                    $e->getProcess()
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function purgeOldFiles(string $backupFilenameTemplate): void
    {
        foreach ($this->folders as $folder) {
            $files = [];
            $directoryIterator = new \DirectoryIterator($folder->getPath());

            foreach ($directoryIterator as $directoryItem) {
                if ($directoryItem->isDot()) {
                    continue;
                }

                $files[] = $directoryItem->getFilename();
            }

            $filesToRemove = $this->getFilesToDelete($files, $backupFilenameTemplate, $folder->getFilesToKeep());

            foreach ($filesToRemove as $file) {
                unlink($folder->getPath().DIRECTORY_SEPARATOR.$file);
            }
        }
    }
}
