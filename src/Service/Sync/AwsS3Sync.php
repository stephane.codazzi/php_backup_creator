<?php

namespace App\Service\Sync;

use App\Exception\ConfigException;
use App\Exception\DependencyException;
use App\Exception\SyncException;
use App\Exception\SystemProcessException;
use App\Model\Sync\AwsS3SyncEnvironment;

class AwsS3Sync extends BaseSync
{
    private const string SERVICE_NAME = 'awsS3Sync';

    private string $executable = '';

    /** @var AwsS3SyncEnvironment[] */
    private array $environments = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['executable'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync need to contain executable field');
        }

        $this->executable = $serviceConfiguration['executable'];

        if (empty($serviceConfiguration['environments']) || !is_array($serviceConfiguration['environments'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync need to contain environments array');
        }

        foreach ($serviceConfiguration['environments'] as $environment) {
            $this->environments[] = $this->parseEnvironment($environment);
        }
    }

    /**
     * @param array<mixed> $environment
     *
     * @throws ConfigException
     */
    private function parseEnvironment(array $environment): AwsS3SyncEnvironment
    {
        $additionalOptions = [];

        if (empty($environment['endpointUrl'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment need to contain endpointUrl');
        }

        if (empty($environment['profile'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment need to contain profile');
        }

        if (empty($environment['source'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment need to contain source');
        }

        if (empty($environment['remote'])) {
            throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment need to contain remote');
        }

        if (!empty($environment['additionalOptions'])) {
            if (!is_array($environment['additionalOptions'])) {
                throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment.additionalOptions need to be a string array');
            }

            foreach ($environment['additionalOptions'] as $additionalOption) {
                if (!is_string($additionalOption)) {
                    throw new ConfigException($this->getServiceName(), 'sync.awsS3Sync.environment.additionalOptions need to be a string array');
                }

                $additionalOptions[] = $additionalOption;
            }
        }

        return new AwsS3SyncEnvironment(
            $environment['source'],
            $environment['remote'],
            $environment['endpointUrl'],
            $environment['profile'],
            $additionalOptions
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
        try {
            $this->systemProcess->execProcessCommandLine(sprintf('type %s > /dev/null', $this->executable));
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                'Missing executable: '.$this->executable,
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );

            $this->notification->captureProcessException(
                $exception,
                $systemProcessException->getProcess()
            );

            throw $exception;
        }
    }

    public function sync(string $backupPath, string $backupFilename): void
    {
        foreach ($this->environments as $environment) {
            try {
                $cmd = [
                    $this->executable,
                    '--endpoint-url',
                    $environment->getEndpointUrl(),
                    '--profile',
                    $environment->getProfile(),
                    's3',
                    'sync',
                    ...$environment->getAdditionalOptions(),
                    $environment->getSource(),
                    $environment->getRemote(),
                ];

                $this->systemProcess->execProcess($cmd, 3600);
            } catch (SystemProcessException $e) {
                $exception = new SyncException(
                    $this->getServiceName(),
                    sprintf(
                        'Cannot sync to remote: %s',
                        $environment->getRemote()
                    ),
                    (int) $e->getCode(),
                    $e
                );

                $this->notification->captureProcessException(
                    $exception,
                    $e->getProcess()
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function purgeOldFiles(string $backupFilenameTemplate): void
    {
        // Nothing to do
    }
}
