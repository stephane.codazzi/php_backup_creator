<?php

namespace App\Service\Sync;

use App\Service\ServiceInterface;

interface SyncInterface extends ServiceInterface
{
    /**
     * Start compression synchronisation.
     */
    public function sync(string $backupPath, string $backupFilename): void;

    /**
     * Purge old files, depending the filesToKeep config parameter.
     */
    public function purgeOldFiles(string $backupFilenameTemplate): void;
}
