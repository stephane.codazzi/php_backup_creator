<?php

namespace App\Service\Sync;

use App\Service\BaseService;

abstract class BaseSync extends BaseService implements SyncInterface
{
    /**
     * {@inheritdoc}
     */
    public function getServiceType(): string
    {
        return self::TYPE_SYNC;
    }

    /**
     * Get files to deletes.
     *
     * @param string[] $files                  Files list, not sorted
     * @param string   $backupFilenameTemplate Backup filename template
     * @param int      $filesToKeep            Number of files to keep in sync service
     *
     * @return string[]
     */
    protected function getFilesToDelete(array $files, string $backupFilenameTemplate, int $filesToKeep): array
    {
        $filesInFolder = 0;
        $filesToRemove = [];

        if ($filesToKeep <= 0) {
            return [];
        }

        rsort($files);

        foreach ($files as $file) {
            if (str_contains($file, $backupFilenameTemplate) && $filesInFolder++ >= $filesToKeep) {
                $filesToRemove[] = $file;
            }
        }

        return $filesToRemove;
    }
}
