<?php

namespace App\Service\Backup;

use App\Exception\BackupException;
use App\Exception\ConfigException;
use App\Exception\SystemProcessException;
use App\Model\Backup\PostgresqlHost;

class PostgresqlBackup extends BaseBackup
{
    public const SERVICE_NAME = 'postgresql';

    public const DATABASE_EXCLUDED = [
        'template0',
        'template1',
    ];

    /**
     * @var PostgresqlHost[]
     */
    private array $hosts = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['hosts']) || !is_array($serviceConfiguration['hosts'])) {
            throw new ConfigException($this->getServiceName(), 'backup.postgresql.hosts must be an array');
        }

        foreach ($serviceConfiguration['hosts'] as $host) {
            $this->createHost($host);
        }
    }

    /**
     * @param array<string> $host
     */
    private function createHost(array $host): void
    {
        if (empty($host['name'])
            || empty($host['host'])
            || empty($host['username'])
            || empty($host['password'])
            || empty($host['database'])
            || empty($host['port'])
        ) {
            throw new ConfigException($this->getServiceName(), 'backup.postgresql.hosts element must have name, database, host, port, username and password.');
        }

        $this->hosts[] = (new PostgresqlHost())
            ->setName($host['name'])
            ->setHost($host['host'])
            ->setUsername($host['username'])
            ->setPassword($host['password'])
            ->setDatabase($host['database'])
            ->setPort($host['port'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
        $this->checkServerExecutable('psql');
    }

    /**
     * {@inheritdoc}
     */
    public function backup(string $backupFolder): void
    {
        $postgresqlDumpFolder = $backupFolder.DIRECTORY_SEPARATOR.self::SERVICE_NAME;

        if (false === mkdir($postgresqlDumpFolder)) {
            throw new BackupException($this->getServiceName(), 'Cannot create folder: '.$postgresqlDumpFolder);
        }

        foreach ($this->hosts as $host) {
            $this->logger->info(sprintf('[POSTGRESQL]: Start dump of: %s', $host->getName()));
            $hostDumpFolder = $postgresqlDumpFolder.DIRECTORY_SEPARATOR.$host->getName();

            if (false === mkdir($hostDumpFolder)) {
                $e = new BackupException($this->getServiceName(), 'Cannot create folder: '.$hostDumpFolder);
                $this->notification->captureException($e);
                continue;
            }

            $postgresqlInfo = sprintf(
                '-U %s -h %s -p %s',
                $host->getUsername(),
                $host->getHost(),
                $host->getPort()
            );
            $showDatabasesCmd = sprintf(
                'PGPASSWORD=%s psql %s -t -c "SELECT datname FROM pg_database;" %s',
                $host->getPassword(),
                $postgresqlInfo,
                $host->getDatabase()
            );
            $dumpCmd = sprintf(
                'while read dbname; do '
                .'PGPASSWORD=%s pg_dump %s -f %s$dbname.sql $dbname'
                .'; done',
                $host->getPassword(),
                $postgresqlInfo,
                $hostDumpFolder.DIRECTORY_SEPARATOR
            );

            try {
                $databases = explode("\n", $this->systemProcess->execProcessCommandLine($showDatabasesCmd));

                foreach ($databases as $databaseLine) {
                    $database = trim($databaseLine);

                    if (!empty($database) && !in_array($database, self::DATABASE_EXCLUDED)) {
                        $dumpCmd = sprintf(
                            'PGPASSWORD=%s pg_dump %s -f %s%s.sql %s',
                            $host->getPassword(),
                            $postgresqlInfo,
                            $hostDumpFolder.DIRECTORY_SEPARATOR,
                            $database,
                            $database
                        );

                        $this->systemProcess->execProcessCommandLine($dumpCmd);
                    }
                }
            } catch (SystemProcessException $e) {
                $this->notification->captureProcessException(
                    new BackupException(
                        $this->getServiceName(),
                        'Cannot dump postgresql: '.$host->getName(),
                        (int) $e->getCode(),
                        $e
                    ),
                    $e->getProcess()
                );
            }
        }
    }
}
