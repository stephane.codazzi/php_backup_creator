<?php

namespace App\Service\Backup;

use App\Exception\BackupException;
use App\Exception\ConfigException;
use App\Exception\SystemProcessException;
use App\Model\Backup\Directory;

class DirectoryBackup extends BaseBackup
{
    public const SERVICE_NAME = 'directory';

    /**
     * @var Directory[]
     */
    private array $directories = [];

    /**
     * {@inheritdoc}
     */
    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['paths'])) {
            throw new ConfigException($this->getServiceName(), 'backup.directory need to contain paths field');
        }

        if (!is_array($serviceConfiguration['paths'])) {
            throw new ConfigException($this->getServiceName(), 'backup.directory.paths must be an array');
        }

        foreach ($serviceConfiguration['paths'] as $path) {
            if (empty($path['backupName']) || empty($path['path'])) {
                throw new ConfigException($this->getServiceName(), 'backup.directory.paths element must have backupName and path.');
            }

            $this->directories[] = (new Directory())
                ->setName($path['backupName'])
                ->setPath($path['path'])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function checkDependencies(): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function backup(string $backupFolder): void
    {
        $serviceDumpFolder = $backupFolder.DIRECTORY_SEPARATOR.self::SERVICE_NAME;

        if (false === mkdir($serviceDumpFolder)) {
            throw new BackupException($this->getServiceName(), 'Cannot create folder: '.$serviceDumpFolder);
        }

        foreach ($this->directories as $directory) {
            $this->logger->info(sprintf('[DIRECTORY]: Start copy of: %s', $directory->getName()));
            $directoryDumpFolder = $serviceDumpFolder.DIRECTORY_SEPARATOR.$directory->getName();

            if (false === mkdir($directoryDumpFolder)) {
                $e = new BackupException($this->getServiceName(), 'Cannot create folder: '.$directoryDumpFolder);
                $this->notification->captureException($e);
                continue;
            }

            try {
                $this->systemProcess->execProcess([
                    'cp',
                    '-r',
                    $directory->getPath(),
                    $directoryDumpFolder,
                ]);
            } catch (SystemProcessException $e) {
                $this->notification->captureProcessException(
                    new BackupException(
                        $this->getServiceName(),
                        'Cannot dump directory: '.$directory->getName(),
                        (int) $e->getCode(),
                        $e
                    ),
                    $e->getProcess()
                );
            }
        }
    }
}
