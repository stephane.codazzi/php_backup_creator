<?php

namespace App\Service\Backup;

use App\Exception\BackupException;
use App\Exception\ConfigException;
use App\Exception\DependencyException;
use App\Exception\SystemProcessException;
use App\Model\Backup\MysqlHost;

/**
 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
 */
class MysqlBackup extends BaseBackup
{
    public const SERVICE_NAME = 'mysql';

    /**
     * @var MysqlHost[]
     */
    private array $hosts = [];

    public function getServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    protected function parseConfiguration(array $serviceConfiguration): void
    {
        if (empty($serviceConfiguration['hosts'])) {
            throw new ConfigException($this->getServiceName(), 'backup.mysql need to contain hosts field');
        }

        if (!is_array($serviceConfiguration['hosts'])) {
            throw new ConfigException($this->getServiceName(), 'backup.mysql.hosts must be an array');
        }

        foreach ($serviceConfiguration['hosts'] as $host) {
            if (empty($host['name'])
                || empty($host['host'])
                || empty($host['username'])
                || empty($host['password'])
            ) {
                throw new ConfigException($this->getServiceName(), 'backup.mysql.hosts element must have name, host, username and password.');
            }

            $ignores = [];

            if (isset($host['ignores'])) {
                if (!is_array($host['ignores'])) {
                    throw new ConfigException($this->getServiceName(), 'backup.mysql.hosts.ignores must be an array');
                }

                $ignores = $host['ignores'];
            }

            $this->hosts[] = new MysqlHost(
                $host['name'],
                $host['username'],
                $host['password'],
                $host['port'] ?? 3306,
                $host['host'],
                $ignores
            );
        }
    }

    protected function checkDependencies(): void
    {
        try {
            $this->systemProcess->execProcessCommandLine('type mysql > /dev/null');
        } catch (SystemProcessException $systemProcessException) {
            $exception = new DependencyException(
                $this->getServiceName(),
                'Missing command: mysql',
                (int) $systemProcessException->getCode(),
                $systemProcessException
            );
            $this->notification->captureProcessException(
                $exception,
                $systemProcessException->getProcess()
            );

            throw $exception;
        }
    }

    public function backup(string $backupFolder): void
    {
        $mysqlDumpFolder = $backupFolder.DIRECTORY_SEPARATOR.self::SERVICE_NAME;

        if (false === mkdir($mysqlDumpFolder)) {
            throw new BackupException($this->getServiceName(), 'Cannot create folder: '.$mysqlDumpFolder);
        }

        foreach ($this->hosts as $host) {
            $this->logger->info(sprintf('[MYSQL]: Start dump of: %s', $host->getName()));
            $databaseDumpFolder = $mysqlDumpFolder.DIRECTORY_SEPARATOR.$host->getName();

            if (false === mkdir($databaseDumpFolder)) {
                $e = new BackupException($this->getServiceName(), 'Cannot create folder: '.$databaseDumpFolder);
                $this->notification->captureException($e);
                continue;
            }

            $mysqlInfo = sprintf(
                '-u %s -p%s -h %s',
                $host->getUsername(),
                $host->getPassword(),
                $host->getHost()
            );

            $ignoresTerms = '';

            if (!empty($host->getIgnores())) {
                $ignoresTerms = sprintf(" WHERE Database NOT IN ('%s')", implode("','", $host->getIgnores()));
            }

            $showDatabasesCmd = sprintf('mysql %s -N -e "show databases%s;"', $mysqlInfo, $ignoresTerms);
            $dumpCmd = sprintf(
                'while read dbname; do '
                .'mysqldump %s --complete-insert --routines --triggers --single-transaction "$dbname"'
                .' > %s"$dbname".sql'
                .'; done',
                $mysqlInfo,
                $databaseDumpFolder.DIRECTORY_SEPARATOR
            );

            try {
                $this->systemProcess->execProcessCommandLine(sprintf('%s | %s', $showDatabasesCmd, $dumpCmd));
            } catch (SystemProcessException $e) {
                $this->notification->captureProcessException(
                    new BackupException(
                        $this->getServiceName(),
                        'Cannot dump mysql: '.$host->getName(),
                        (int) $e->getCode(),
                        $e
                    ),
                    $e->getProcess()
                );
            }
        }
    }
}
