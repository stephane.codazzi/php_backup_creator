<?php

namespace App\Service\Backup;

use App\Service\BaseService;

abstract class BaseBackup extends BaseService implements BackupInterface
{
    /**
     * {@inheritdoc}
     */
    public function getServiceType(): string
    {
        return self::TYPE_BACKUP;
    }
}
