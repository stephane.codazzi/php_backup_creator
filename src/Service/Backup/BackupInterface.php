<?php

namespace App\Service\Backup;

use App\Service\ServiceInterface;

interface BackupInterface extends ServiceInterface
{
    /**
     * Start backup service.
     */
    public function backup(string $backupFolder): void;
}
