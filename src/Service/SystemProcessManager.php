<?php

namespace App\Service;

use App\Exception\SystemProcessException;
use Symfony\Component\Process\Process;

class SystemProcessManager
{
    /**
     * @param array<string|int> $cmd
     * @param string[]          $env
     *
     * @return string Command Output
     *
     * @throws SystemProcessException
     */
    public function execProcess(array $cmd, int $timeToLive = 18000, array $env = [], ?string $cwd = null): string
    {
        $process = new Process($cmd, $cwd);
        $process->setTimeout($timeToLive);
        $process->setEnv($env);
        $process->run();

        if (!$process->isSuccessful() || !empty($process->getErrorOutput())) {
            throw new SystemProcessException($process);
        }

        return $process->getOutput();
    }

    /**
     * @param string[] $env
     *
     * @return string Command output
     *
     * @throws SystemProcessException
     */
    public function execProcessCommandLine(
        string $cmd,
        int $timeToLive = 18000,
        array $env = [],
        ?string $cwd = null,
    ): string {
        $process = Process::fromShellCommandline($cmd, $cwd);
        $process->setTimeout($timeToLive);
        $process->setEnv($env);
        $process->run();

        if (!$process->isSuccessful() || !empty($process->getErrorOutput())) {
            throw new SystemProcessException($process);
        }

        return $process->getOutput();
    }
}
