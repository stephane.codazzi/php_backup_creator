<?php

namespace App\Service;

use App\Exception\ConfigException;

interface ServiceInterface
{
    /**
     * Function used to load configuration and check dependencies if service is enabled.
     *
     * @param array<mixed> $configuration
     *
     * @throws ConfigException
     */
    public function load(array $configuration): void;

    /**
     * Return the service name.
     */
    public function getServiceName(): string;

    /**
     * Return the service type.
     */
    public function getServiceType(): string;

    /**
     * Return is the current service is enabled.
     */
    public function isEnable(): bool;
}
