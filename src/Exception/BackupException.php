<?php

namespace App\Exception;

class BackupException extends \Exception
{
    /**
     * BackupException constructor.
     */
    public function __construct(string $serviceName, string $message, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('[BACKUP][%s]: %s', $serviceName, $message), $code, $previous);
    }
}
