<?php

namespace App\Exception;

use Symfony\Component\Process\Process;

class SystemProcessException extends \Exception
{
    /**
     * SystemProcessException constructor.
     */
    public function __construct(private readonly Process $process)
    {
        parent::__construct($this->process->getErrorOutput());
    }

    public function getProcess(): Process
    {
        return $this->process;
    }
}
