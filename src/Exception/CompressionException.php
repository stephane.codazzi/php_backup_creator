<?php

namespace App\Exception;

class CompressionException extends \Exception
{
    /**
     * BackupException constructor.
     */
    public function __construct(string $serviceName, string $message, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('[COMPRESSION][%s]: %s', $serviceName, $message), $code, $previous);
    }
}
