<?php

namespace App\Exception;

class ConfigException extends \Exception
{
    /**
     * ConfigException constructor.
     */
    public function __construct(string $serviceName, string $message, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('[CONFIG][%s]: %s', $serviceName, $message), $code, $previous);
    }
}
