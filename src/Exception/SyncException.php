<?php

namespace App\Exception;

class SyncException extends \Exception
{
    /**
     * SyncException constructor.
     */
    public function __construct(string $serviceName, string $message, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('[SYNC][%s]: %s', $serviceName, $message), $code, $previous);
    }
}
