<?php

namespace App\Exception;

class DependencyException extends \Exception
{
    /**
     * DependencyException constructor.
     */
    public function __construct(string $serviceName, string $message, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('[DEPENDENCY][%s]: %s', $serviceName, $message), $code, $previous);
    }
}
