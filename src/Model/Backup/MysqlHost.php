<?php

namespace App\Model\Backup;

readonly class MysqlHost
{
    /**
     * @param string[] $ignores
     */
    public function __construct(
        private string $name,
        private string $username,
        private string $password,
        private string $port,
        private string $host,
        private array $ignores,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @return string[]
     */
    public function getIgnores(): array
    {
        return $this->ignores;
    }
}
