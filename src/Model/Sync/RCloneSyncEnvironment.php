<?php

namespace App\Model\Sync;

class RCloneSyncEnvironment
{
    /**
     * @param string[] $additionalOptions
     */
    public function __construct(
        private readonly string $configFilePath,
        private readonly string $source,
        private readonly string $remote,
        private readonly string $destination,
        private readonly array $additionalOptions,
    ) {
    }

    public function getConfigFilePath(): string
    {
        return $this->configFilePath;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getRemote(): string
    {
        return $this->remote;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    /**
     * @return string[]
     */
    public function getAdditionalOptions(): array
    {
        return $this->additionalOptions;
    }
}
