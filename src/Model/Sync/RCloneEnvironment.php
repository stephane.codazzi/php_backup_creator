<?php

namespace App\Model\Sync;

class RCloneEnvironment
{
    /**
     * @param string[] $additionalOptions
     */
    public function __construct(
        private readonly string $configFilePath,
        private readonly string $remote,
        private readonly string $destination,
        private readonly array $additionalOptions,
        private readonly int $filesToKeep = 1,
    ) {
    }

    public function getConfigFilePath(): string
    {
        return $this->configFilePath;
    }

    public function getRemote(): string
    {
        return $this->remote;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    /**
     * @return string[]
     */
    public function getAdditionalOptions(): array
    {
        return $this->additionalOptions;
    }

    public function getFilesToKeep(): int
    {
        return $this->filesToKeep;
    }
}
