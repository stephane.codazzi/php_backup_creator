<?php

namespace App\Model\Sync;

class Directory
{
    private string $path = '';

    private int $filesToKeep = 1;

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function getFilesToKeep(): int
    {
        return $this->filesToKeep;
    }

    public function setFilesToKeep(int $filesToKeep): void
    {
        $this->filesToKeep = $filesToKeep;
    }
}
