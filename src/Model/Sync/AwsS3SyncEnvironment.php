<?php

namespace App\Model\Sync;

class AwsS3SyncEnvironment
{
    /**
     * @param string[] $additionalOptions
     */
    public function __construct(
        private readonly string $source,
        private readonly string $remote,
        private readonly string $endpointUrl,
        private readonly string $profile,
        private readonly array $additionalOptions,
    ) {
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getRemote(): string
    {
        return $this->remote;
    }

    public function getEndpointUrl(): string
    {
        return $this->endpointUrl;
    }

    public function getProfile(): string
    {
        return $this->profile;
    }

    /**
     * @return string[]
     */
    public function getAdditionalOptions(): array
    {
        return $this->additionalOptions;
    }
}
