<?php

namespace App\Command;

use App\Service\BackupManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'backup',
    description: 'Start backup',
)]
class BackupCommand extends Command
{
    public function __construct(private readonly LoggerInterface $logger, private readonly BackupManager $backupManager, ?string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption('config', 'c', InputOption::VALUE_OPTIONAL, 'Config file path', 'config.yml')
        ;
    }

    /**
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info('Start backup');
        $configPath = $input->getOption('config');

        if (!is_string($configPath)) {
            throw new \Exception('Cannot get config path');
        }

        $this->backupManager->startBackup($configPath);

        return Command::SUCCESS;
    }
}
