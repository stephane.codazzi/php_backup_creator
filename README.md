## Getting started

- Clone the project: `git clone git@gitlab.com:stephane.codazzi/php_backup_creator.git`
- Install composer dependencies: `composer install --no-dev`
- Copy, and edit the `config.yml.dist`
- Create your first backup: `php bin/console backup -c config.yml`
- Create a file .env.local with:
```yaml
APP_ENV=prod
APP_SECRET=YourSecretString
SENTRY_DSN= # Keep empty if not used
NOTIFICATION_EMAILS= # Send email if backup fail. Use comma to add emails
MAILER_DSN= # Add mailer DSN to use mail notification. Keep empty if not used
MAILER_FROM= # Mail sender
```

## Run backup creator with docker:

```shell
docker run \
 -v /my_path/myconfig.yml:/config.yml \
 -v /my_path/backups:/backups \
 -w /var/www \
 registry.gitlab.com/stephane.codazzi/php_backup_creator/backup:master \
 php bin/console backup -c /config.yml -vvv
```

## Available services (See config.yml.dist for configuration example)
### Backup services

- MySQL: Backup all tables (1 table per file)
- Postgresql: Backup all tables (1 table per file)
- Local folder: Backup a folder content

### Compression services

- Zip
- P7zip

### Sync services

- AWS S3 Sync: Used only with "skip" compression. Create a mirror of source folder
- Local folder: create a local copy
- Rclone: send current backup with rclone configuration, and delete old backups
- RcloneSync: Used only with "skip" compression. Create a mirror of source folder

## Roadmap

- Add mongodb backup service
