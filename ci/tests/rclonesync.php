<?php

$expectedOutput = 'importPostgresql.shmysql.sqlpostgresql.sql';
$command = '/usr/bin/rclone --config ci/configs/rclone.conf lsf swift-server:rclonesync';
$output = [];
$errorCode = 0;

exec($command, $output, $errorCode);

$stringOutput = implode('', $output);

if ($expectedOutput !== $stringOutput) {
    throw new Exception(sprintf('Output not expected ! Received : %d expected : %s', $stringOutput, $expectedOutput));
}
