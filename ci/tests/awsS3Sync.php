<?php

$outputExpectContaining = ['307 importPostgresql.sh', '2718 mysql.sql', '3742 postgresql.sql'];

$command = '/usr/bin/aws --endpoint-url=http://localstack:4566 s3 ls backups';
$output = [];
$errorCode = 0;

exec($command, $output, $errorCode);

$stringOutput = implode('', $output);

foreach ($outputExpectContaining as $term) {
    if (!str_contains($stringOutput, $term)) {
        throw new Exception(sprintf('Output not expected ! Received : %d expected : %s', $stringOutput, $term));
    }
}
